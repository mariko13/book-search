import React, {Fragment, useContext} from 'react'
import {Search} from '../components/Search'
import {Card} from '../components/Card'
import {BooksContext, IBookInfo, IBooksContext} from '../context/books/booksContext'

export const Home = () => {
    const {loading, books}: Partial<IBooksContext> = useContext(BooksContext)

    return (
        <Fragment>
            <Search/>
            <div className="row">
                {loading && <p className="text-center">Loading...</p>}
                {books && books.map((book: IBookInfo) => (
                    <div className="col-sm-4 mb-4" key={book.id}>
                        <Card book={book}/>
                    </div>))
                }
            </div>
        </Fragment>
    )
}