import React, {useContext, Fragment} from 'react'
import {BooksContext, IBooksContext} from '../context/books/booksContext'
import {Link} from 'react-router-dom'

export const BooksInfo = () => {
    const {loading, book}: Partial<IBooksContext> = useContext(BooksContext)

    if (loading) {
        return <p className="text-center">Loading...</p>
    }

    if (!book || Object.keys(book).length === 0) return <></>

    const {volumeInfo} = book

    return (
        <Fragment>
            <Link to="/" className="btn btn-link">На главную</Link>

            <div className="card mb-4">
                <div className="card-body">
                    <div className="row">
                        <div className="col-sm-3 mx-auto">
                            <img src={volumeInfo.imageLinks.thumbnail} alt={volumeInfo.title}/>
                            <h1>{volumeInfo.title}</h1>
                            <p>{volumeInfo.authors}</p>
                            <p>Дата выхода: {volumeInfo.publishedDate}</p>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}