import {HIDE_ALERT, SHOW_ALERT} from '../types'

interface IAction {
    type: "SHOW_ALERT" | "HIDE_ALERT" | "DEFAULT"
    payload?: any
}

const handlers = {
    [SHOW_ALERT]: (state: any, action: IAction) => action.payload,
    [HIDE_ALERT]: () => null,
    DEFAULT: (state: any) => state
}

export const alertReducer = (state: any, action: IAction) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}