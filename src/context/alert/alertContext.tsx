import {createContext} from 'react'

export interface IAlertContext {
    hide: () => void
    show: (text: string, type?: string) => void
    alert: any
}

export const AlertContext = createContext<Partial<IAlertContext>>({})