export const SHOW_ALERT = 'SHOW_ALERT'
export const HIDE_ALERT = 'HIDE_ALERT'
export const CLEAR_BOOKS = 'CLEAR_BOOKS' //очистить список
export const SET_BOOK = 'SET_BOOK' //конкретная книга
export const SEARCH_BOOKS = 'SEARCH_BOOKS' //список книг
export const SET_LOADING = 'SET_LOADING' //загрузка

