import {createContext} from 'react'

export interface IImageLinks {
    smallThumbnail?: string
    thumbnail: string
}

export interface IVolumeInfo {
    title: string
    authors: string[]
    publishedDate: string | number
    imageLinks: IImageLinks
}

export interface ISearchInfo {
    textSnippet: string
}

export interface IBookInfo {
    id?: string
    volumeInfo: IVolumeInfo
    searchInfo: ISearchInfo
}

export interface IBooksContext {
    search: (value: string) => void
    setBook: (book: IBookInfo) => void
    clearBooks: () => void
    setLoading: () => void
    book: IBookInfo
    books: IBookInfo[]
    loading: any
}

export const BooksContext = createContext<Partial<IBooksContext>>({})