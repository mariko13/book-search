import React, {useReducer} from 'react'
import axios from 'axios'
import {BooksContext, IBookInfo} from './booksContext'
import {booksReducer} from './booksReducer'
import {CLEAR_BOOKS, SEARCH_BOOKS, SET_LOADING, SET_BOOK} from '../types'

const API_KEY = process.env.REACT_APP_API_KEY

interface Props {
    children: JSX.Element
}

interface IInitialState {
    book: IBookInfo | null
    books: IBookInfo[] | null
    loading: boolean
}

export const BooksState = ({children}: Props) => {
    const initialState: IInitialState = {
        book: null,
        books: null,
        loading: false,
    }
    const [state, dispatch] = useReducer(booksReducer, initialState)

    const search = async (value: string) => {
        setLoading()

        const response = await axios.get(`https://www.googleapis.com/books/v1/volumes?q=${value}&key=${API_KEY}`)

        dispatch({
            type: SEARCH_BOOKS,
            payload: response.data.items
        })
    }

    const setBook = async (book: IBookInfo) => {
        setLoading()

        dispatch({
            type: SET_BOOK,
            payload: book
        })
    }

    const clearBooks = () => dispatch({type: CLEAR_BOOKS})

    const setLoading = () => dispatch({type: SET_LOADING})

    const {book, books, loading} = state

    return (
        <BooksContext.Provider value={{
            search, setBook, clearBooks, setLoading,
            book, books, loading
        }}>
            {children}
        </BooksContext.Provider>
    )
}


