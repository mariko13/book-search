import {CLEAR_BOOKS, SEARCH_BOOKS, SET_LOADING, SET_BOOK} from '../types'

interface IAction {
    type: "SEARCH_BOOKS" | "SET_BOOK" | "SET_LOADING" | "CLEAR_BOOKS" | "DEFAULT"
    payload?: any
}

const handlers = {
    [SEARCH_BOOKS]: (state: any, action: IAction) => ({...state, books: action.payload, loading: false}),
    [SET_BOOK]: (state: any, action: IAction) => ({...state, book: action.payload, loading: false}),
    [SET_LOADING]: (state: any) => ({...state, loading: true}),
    [CLEAR_BOOKS]: (state: any) => ({...state, books: []}),
    DEFAULT: (state: any) => state
}

export const booksReducer = (state: any, action: IAction) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}