import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import {Navbar} from './components/Navbar'
import {Home} from './pages/Home'
import {BooksInfo} from './pages/BooksInfo'
import {Alert} from './components/Alert'
import {AlertState} from './context/alert/AlertState'
import {BooksState} from './context/books/BooksState'

function App() {
    return (
        <BooksState>
            <AlertState>
                <BrowserRouter>
                    <Navbar/>
                    <div className="container pt-4">
                        <Alert/>
                        <Switch>
                            <Route path="/" exact component={Home}/>
                            <Route path="/books_info/:name" component={BooksInfo}/>
                        </Switch>
                    </div>
                </BrowserRouter>
            </AlertState>
        </BooksState>
    )
}

export default App