import React, {useContext, useState} from 'react'
import {AlertContext, IAlertContext} from '../context/alert/alertContext'
import {BooksContext, IBooksContext} from '../context/books/booksContext'

export const Search = () => {
    const [value, setValue] = useState('')
    const {show}: Partial<IAlertContext> = useContext(AlertContext)
    const {search}: Partial<IBooksContext> = useContext(BooksContext)

    const onSubmit = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key !== 'Enter') {
            return
        }

        if (value.trim() && search) {
            search(value.trim())
        } else {
            if (show) {
                show('Введите название книги')
            }
        }
    }

    return (
        <div className="form-group mb-4">
            <input
                type="text"
                className="form-control"
                placeholder="Введите название книги"
                value={value}
                onChange={event => setValue(event.target.value)}
                onKeyPress={onSubmit}
            />
        </div>
    )
}