import React, {useContext} from 'react'
import {useHistory} from 'react-router-dom'
import {BooksContext, IBookInfo, IBooksContext} from '../context/books/booksContext'

interface ICard {
    book: IBookInfo
}

export const Card = ({book}: ICard) => {
    const {setBook}: Partial<IBooksContext> = useContext(BooksContext)
    const history = useHistory()

    if (!book || Object.keys(book).length === 0 || !setBook) return <></>

    const openBook = (book: IBookInfo) => {
        setBook(book)
        history.push('/books_info/' + book.volumeInfo.title)
    }

    return (
        <div className="card text-center">
            <div className="mx-auto" style={{width: "128px", height: "200px"}}>
                <img src={book.volumeInfo.imageLinks.thumbnail} alt={''} className="card-img-top img-thumbnail w-100"/>
            </div>
            <div className="card-body">
                <h5 className="card-title">{book.volumeInfo.title}</h5>
                <p>{book.volumeInfo.authors}</p>
                <button onClick={() => openBook(book)} className="btn btn-primary">Открыть</button>
            </div>
        </div>
    )
}